﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnContact : MonoBehaviour {
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreVal;
    private GameController gameController;
    private GameObject[] userSong;
    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if(gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        else
        {
            Debug.Log("Can't find game controller");
        }
    }
    private void OnTriggerEnter(Collider other)
    
    {
    if(other.CompareTag("Boundary") || other.CompareTag("Enemy"))
        {

            return;
        }
    if(explosion != null)
        {
            if (other.tag == "Player")
            {
                return;
            }
            Instantiate(explosion, transform.position, transform.rotation); 
        }
    
        Destroy(other.gameObject);
        Destroy(gameObject);
        
        
        Debug.Log("name of object" + this.name);
        if(this.name == "Note A(Clone)")
        {
            gameController.addNote("a");
        
        }
        else if(this.name == "Note B(Clone)")
        {
            gameController.addNote("b");
        }
        else if(this.name == "Note C(Clone)")
        {
            gameController.addNote("c");
        }
        else if(this.name == "Note D(Clone)")
        {
            gameController.addNote("d");
        }
        else if (this.name == "Note E(Clone)")
        {
            gameController.addNote("e");
        }
        else if (this.name == "Note F(Clone)")
        {
            gameController.addNote("f");
            
        }
        else if (this.name == "Note G(Clone)")
        {
            gameController.addNote("g");
        }
        gameController.checkSong();


    }
}

