﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameController : MonoBehaviour {
    public GameObject[] hazard;

    public AudioClip[] notes;
    public AudioSource audio1;
    public Vector3 spawnValue;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    private int level;

    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    public Text songText;
    public Text songToPlay;

    public int score = 0;
    private static int currLevel;
    
    private string song = "";
    private bool gameOver;
    private bool restart;
    private bool nextLevel;
    private bool levelOver;
    private bool callSong = false;
    private bool levelPass = false;

	// Use this for initialization
	void Start () {
        
        gameOver = false;
        restart = false;
        levelOver = false;
        
        level = 0;
        StartCoroutine (spawnWaves());
        
        //currLevel = PlayerPrefs.GetInt("Level");
        
        UpdateSong();
        restartText.text = "";
        gameOverText.text = "";
        scoreText.text = "";
        Debug.Log(currLevel);

        if(level == 0 && !(currLevel == 1) && !(currLevel == 2))
        {
            songToPlay.text = "a";
        }
        else if(currLevel == 1)
        {
            songToPlay.text = "bagbag";//"bagbag"
        }
        else if(currLevel == 2)
        {
            songToPlay.text = "cccgaageed";
        }
    }
	
	// Update is called once per frame
	void Update () {
        restartText.text = "Press 'R' to restart";
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);
            }
        
        if(levelOver)

        {
            if (callSong == false)
            {
                StartCoroutine(playAudioSequentially());
                callSong = true;
            }
            

            Debug.Log("made it to ending the level");
           

            if (levelPass)
            {
                restartText.text = "Press 'Space' for next level";
                scoreText.text = "You Passed, wait to listen to your song";
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    currLevel++;
                    SceneManager.LoadScene("SampleScene", LoadSceneMode.Single);

                }
            }
            else
            {
                scoreText.text = "You Lost, Try Again";
            }
                
            
        }
	}

     IEnumerator spawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true) {
            for (int i = 0; i < hazardCount; i++)
            {
                Vector3 spawnPos = new Vector3(UnityEngine.Random.Range(-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
                Quaternion spawnRot = Quaternion.identity;
                Instantiate(hazard[UnityEngine.Random.Range(0,7)], spawnPos, spawnRot);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
            
        }
        
    }

    IEnumerator playAudioSequentially()
        
    {

        
            yield return new WaitForSeconds(3);

            Char[] endLevelSong = song.ToCharArray();
            //1.Loop through each AudioClip
            for (int i = 0; i < endLevelSong.Length; i++)
            {
                Debug.Log("made it to the for loop");
                Char currChar = (Char)endLevelSong.GetValue(i);
                if (currChar == 'a')
                {
                    audio1.clip = (AudioClip)notes.GetValue(0);
                    if (!audio1.isPlaying)
                    {
                        audio1.Play();
                    }

                    Debug.Log("played clip");
                }
                else if (currChar == 'b')
                {
                    audio1.clip = (AudioClip)notes.GetValue(1);
                    if (!audio1.isPlaying)
                    {
                        audio1.Play();
                    }
                }
                else if (currChar == 'c')
                {
                    audio1.clip = (AudioClip)notes.GetValue(2);
                    if (!audio1.isPlaying)
                    {
                        audio1.Play();
                    }
                }
                else if (currChar == 'd')
                {
                    audio1.clip = (AudioClip)notes.GetValue(3);
                    if (!audio1.isPlaying)
                    {
                        audio1.Play();
                    }
                }
                else if (currChar == 'e')
                {
                    audio1.clip = (AudioClip)notes.GetValue(4);
                    if (!audio1.isPlaying)
                    {
                        audio1.Play();
                    }
                }
                else if (currChar == 'f')
                {
                    audio1.clip = (AudioClip)notes.GetValue(5);
                    if (!audio1.isPlaying)
                    {
                        audio1.Play();
                    }
                }
                else if (currChar == 'g')
                {
                    audio1.clip = (AudioClip)notes.GetValue(6);
                    if (!audio1.isPlaying)
                    {
                        audio1.Play();
                    }
                }
                yield return new WaitForSeconds(audio1.clip.length);



                
            }
        
    }

    public void AddScore(int newScore)
    {
        score += newScore;
        updateScore();
    }

    void updateScore()
    {
        
        scoreText.text = "Score: " + score;
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }

    public void UpdateSong()
    {
        songText.text = song;
        
        
    }
    public void addNote(string note)
    {
        song = song + note;
        UpdateSong();
    }
    public void checkSong()
    { 
        if(songText.text.Length == songToPlay.text.Length)
        {
            levelOver = true;
            if (songToPlay.text.Equals(songText.text))
            {
                levelPass = true;
            }
        }
        Debug.Log("player song" + songText.text);
        Debug.Log("do songs equal?" + songToPlay.text.Equals(songText.text));
        
       
        
    }

}
