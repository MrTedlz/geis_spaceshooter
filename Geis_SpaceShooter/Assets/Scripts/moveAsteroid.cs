﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveAsteroid : MonoBehaviour {

    public float tumble;
    private void Start()
    {
        Rigidbody body = GetComponent<Rigidbody>();
        body.angularVelocity = Random.insideUnitSphere * tumble;
    }
}
