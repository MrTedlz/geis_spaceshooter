﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBolt : MonoBehaviour {
    public float speed;
	// Use this for initialization
	void Start () {
        Rigidbody body = GetComponent<Rigidbody>();
        body.velocity = body.transform.forward * speed;
	}
	
	
}
