﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}
public class playerController : MonoBehaviour {
    public float speed;
    public float tilt;
    public Boundary boundary;

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    private float nextFire;

    private void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
        }

       
    }
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Rigidbody body = GetComponent<Rigidbody>();

        Vector3 move = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        body.velocity = move * speed;

        body.position = new Vector3(
            Mathf.Clamp(
                body.position.x,
                boundary.xMin, 
                boundary.xMax
                )
             ,0.0f
             , Mathf.Clamp(
                 body.position.z, boundary.zMin, boundary.zMax
                 ));
        body.rotation = Quaternion.Euler(0.0f, 0.0f, body.velocity.x * -tilt);
    }
}
